#!/bin/bash
. ~/producers/.kafka_envs

cd ~/producers/energy/alfen-charging-station
. env/bin/activate
python serializing_producer.py
