from confluent_kafka import Producer
from confluent_kafka.serialization import StringSerializer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.serialization import SerializationContext, MessageField

import alfen_eve_modbus_tcp

import requests
import time
import logging

import projectsecrets
from tgvfunctions import tgvfunctions

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.ERROR
)
logger = logging.getLogger(__file__)


description_mapping = {
    # 'meter_state': 'Meter State [1] Initialised [2] Updated [3] Initialised & Updated [4] Warning [5] Warning & Initialised [6] Warning & Updated [7] Warning, Initialised & Updated [8] Error [9] Error & Initialised [10] Error & Updated [11] Error, Initialised & Updated [12] Error & Warning [13] Error, Warning & Initialised [14] Error, Warning & Updated [15] Error, Warning, Initialised & Updated',
    'mode_3_state': '[A] NotConnected [B1] Connected [B2] Connected [C1] Connected [C2] Charging [D1] Connected [D2] Charging [E] NotConnected [F] Error',
    'availability': '[1] Operable [0] Inoperable',
    'actual_applied_max_current': 'The actual maximum current that the socket can deliver',
    'active_load_balancing_safe_current': 'The fallback maximum current if there is no load balancing active',
    'modbus_slave_max_current': 'The maximum current setpoint, set through Modbus',
    'station_active_max_current': 'Station maximum current setpoint, set through ACE Service Installer',
}

key_mapping = {
    'active_load_balancing_safe_current': 'Active Load Balancing Safe Current',
    'actual_applied_max_current': 'Max Current - Actual Applied',
    # 'apparent_energy_phase_L1': 'Apparent Energy Phase L1',
    # 'apparent_energy_phase_L2': 'Apparent Energy Phase L2',
    # 'apparent_energy_phase_L3': 'Apparent Energy Phase L3',
    # 'apparent_energy_sum': 'Apparent Energy Sum',
    # 'apparent_power_phase_L1': 'Apparent Power Phase L1',
    # 'apparent_power_phase_L2': 'Apparent Power Phase L2',
    # 'apparent_power_phase_L3': 'Apparent Power Phase L3',
    # 'apparent_power_sum': 'Apparent Power Sum',
    'availability': 'Availability',
    # 'c_date_day': 'Day',
    # 'c_date_month': 'Month',
    # 'c_date_year': 'Year',
    # 'c_firmware_version': 'Firmware Version',
    # 'c_manufacturer': 'Manufacturer',
    # 'c_modbus_table_version': 'Modbus Table Version',
    # 'c_name': 'Name',
    # 'c_platform_type': 'Platform Type',
    # 'c_station_serial_number': 'Station Serial Number',
    # 'c_time_hour': 'Time Hour',
    # 'c_time_minute': 'Time Minute',
    # 'c_time_second': 'Time Second',
    # 'c_time_zone': 'Time Zone',
    # 'c_uptime': 'Uptime',
    # 'charge_using_1_or_3_phases': 'Charge Using 1 or 3 Phases',
    # 'current_N': 'Current N',
    'current_phase_L1': 'Current Phase L1',
    # 'current_phase_L2': 'Current Phase L2',
    # 'current_phase_L3': 'Current Phase L3',
    # 'current_sum': 'Current Sum',
    'frequency': 'Frequency',
    # 'meter_last_value_timestamp': 'Meter Last Value Timestamp',
    # 'meter_state': 'Meter State',
    # 'meter_type': 'Meter Type',
    'modbus_slave_max_current': 'Maximum Current - Modbus Setpoint',
    # 'modbus_slave_max_current_valid_time': 'Modbus Slave Max Current Valid Time',
    # 'modbus_slave_received_setpoint_accounted_for': 'Modbus Slave Received Setpoint Accounted For',
    'mode_3_state': 'Mode 3 State',
    # 'nr_of_sockets': 'Number of Sockets',
    # 'ocpp_state': 'OCPP State',
    # 'power_factor_phase_L1': 'Power Factor Phase L1',
    # 'power_factor_phase_L2': 'Power Factor Phase L2',
    # 'power_factor_phase_L3': 'Power Factor Phase L3',
    'power_factor_sum': 'Power Factor Sum',
    # 'reactive_energy_phase_L1': 'Reactive Energy Phase L1',
    # 'reactive_energy_phase_L2': 'Reactive Energy Phase L2',
    # 'reactive_energy_phase_L3': 'Reactive Energy Phase L3',
    # 'reactive_energy_sum': 'Reactive Energy Sum',
    # 'reactive_power_phase_L1': 'Reactive Power Phase L1',
    # 'reactive_power_phase_L2': 'Reactive Power Phase L2',
    # 'reactive_power_phase_L3': 'Reactive Power Phase L3',
    # 'real_energy_consumed_phase_L1': 'Real Energy Consumed Phase L1',
    # 'real_energy_consumed_phase_L2': 'Real Energy Consumed Phase L2',
    # 'real_energy_consumed_phase_L3': 'Real Energy Consumed Phase L3',
    # 'real_energy_consumed_sum': 'Real Energy Consumed Sum',
    # 'real_energy_delivered_phase_L1': 'Real Energy Delivered Phase L1',
    # 'real_energy_delivered_phase_L2': 'Real Energy Delivered Phase L2',
    # 'real_energy_delivered_phase_L3': 'Real Energy Delivered Phase L3',
    'real_energy_delivered_sum': 'Real Energy Delivered Sum',
    # 'real_power_phase_L1': 'Real Power Phase L1',
    # 'real_power_phase_L2': 'Real Power Phase L2',
    # 'real_power_phase_L3': 'Real Power Phase L3',
    'real_power_sum': 'Real Power Sum',
    # 'remaining_valid_time_max_current_phase_l1': 'Remaining Valid Time Max Current Phase L1',
    # 'remaining_valid_time_max_current_phase_l2': 'Remaining Valid Time Max Current Phase L2',
    # 'remaining_valid_time_max_current_phase_l3': 'Remaining Valid Time Max Current Phase L3',
    # 'scn_actual_max_current_phase_l1': 'SCN Actual Max Current Phase L1',
    # 'scn_actual_max_current_phase_l2': 'SCN Actual Max Current Phase L2',
    # 'scn_actual_max_current_phase_l3': 'SCN Actual Max Current Phase L3',
    # 'scn_max_current_phase_l1': 'SCN Max Current Phase L1',
    # 'scn_max_current_phase_l2': 'SCN Max Current Phase L2',
    # 'scn_max_current_phase_l3': 'SCN Max Current Phase L3',
    # 'scn_modbus_slave_max_current_enable': 'SCN Modbus Slave Max Current Enable',
    # 'scn_name': 'SCN Name',
    # 'scn_safe_current': 'SCN Safe Current',
    # 'scn_sockets': 'SCN Sockets',
    # 'scn_total_consumption_phase_l1': 'SCN Total Consumption Phase L1',
    # 'scn_total_consumption_phase_l2': 'SCN Total Consumption Phase L2',
    # 'scn_total_consumption_phase_l3': 'SCN Total Consumption Phase L3',
    # 'station_active_max_current': 'Station Active Max Current',
    'temperature': 'Temperature',
    # 'voltage_phase_L1L2': 'Voltage Phase L1L2',
    'voltage_phase_L1N': 'Voltage Phase L1N',
    # 'voltage_phase_L2L3': 'Voltage Phase L2L3',
    # 'voltage_phase_L2N': 'Voltage Phase L2N',
    # 'voltage_phase_L3L1': 'Voltage Phase L3L1',
    # 'voltage_phase_L3N': 'Voltage Phase L3N'
}

unit_mapping = {
    'active_load_balancing_safe_current': 'A',
    'actual_applied_max_current': 'A',
    'apparent_energy_phase_L1': 'VAh',
    'apparent_energy_phase_L2': 'VAh',
    'apparent_energy_phase_L3': 'VAh',
    'apparent_energy_sum': 'VAh',
    'apparent_power_phase_L1': 'VA',
    'apparent_power_phase_L2': 'VA',
    'apparent_power_phase_L3': 'VA',
    'apparent_power_sum': 'VA',
    'availability': '',
    'c_date_day': 'd',
    'c_date_month': 'mon',
    'c_date_year': 'yr',
    'c_firmware_version': '',
    'c_manufacturer': '',
    'c_modbus_table_version': '',
    'c_name': '',
    'c_platform_type': '',
    'c_station_serial_number': '',
    'c_time_hour': 'h',
    'c_time_minute': 'min',
    'c_time_second': 's',
    'c_time_zone': '',
    'c_uptime': 's',
    'charge_using_1_or_3_phases': '',
    'current_N': 'A',
    'current_phase_L1': 'A',
    'current_phase_L2': 'A',
    'current_phase_L3': 'A',
    'current_sum': 'A',
    'frequency': 'Hz',
    'meter_last_value_timestamp': 's',
    'meter_state': '',
    'meter_type': '',
    'modbus_slave_max_current': 'A',
    'modbus_slave_max_current_valid_time': 's',
    'modbus_slave_received_setpoint_accounted_for': '',
    'mode_3_state': '',
    'nr_of_sockets': '',
    'ocpp_state': '',
    'power_factor_phase_L1': '',
    'power_factor_phase_L2': '',
    'power_factor_phase_L3': '',
    'power_factor_sum': '',
    'reactive_energy_phase_L1': 'VArh',
    'reactive_energy_phase_L2': 'VArh',
    'reactive_energy_phase_L3': 'VArh',
    'reactive_energy_sum': 'VArh',
    'reactive_power_phase_L1': 'VAr',
    'reactive_power_phase_L2': 'VAr',
    'reactive_power_phase_L3': 'VAr',
    'reactive_power_sum': 'VAr',
    'real_energy_consumed_phase_L1': 'Wh',
    'real_energy_consumed_phase_L2': 'Wh',
    'real_energy_consumed_phase_L3': 'Wh',
    'real_energy_consumed_sum': 'Wh',
    'real_energy_delivered_phase_L1': 'Wh',
    'real_energy_delivered_phase_L2': 'Wh',
    'real_energy_delivered_phase_L3': 'Wh',
    'real_energy_delivered_sum': 'Wh',
    'real_power_phase_L1': 'W',
    'real_power_phase_L2': 'W',
    'real_power_phase_L3': 'W',
    'real_power_sum': 'W',
    'remaining_valid_time_max_current_phase_l1': 's',
    'remaining_valid_time_max_current_phase_l2': 's',
    'remaining_valid_time_max_current_phase_l3': 's',
    'scn_actual_max_current_phase_l1': 'A',
    'scn_actual_max_current_phase_l2': 'A',
    'scn_actual_max_current_phase_l3': 'A',
    'scn_max_current_phase_l1': 'A',
    'scn_max_current_phase_l2': 'A',
    'scn_max_current_phase_l3': 'A',
    'scn_modbus_slave_max_current_enable': '',
    'scn_name': '',
    'scn_safe_current': 'A',
    'scn_sockets': '',
    'scn_total_consumption_phase_l1': 'Wh',
    'scn_total_consumption_phase_l2': 'Wh',
    'scn_total_consumption_phase_l3': 'Wh',
    'station_active_max_current': 'A',
    'temperature': '°C',
    'voltage_phase_L1L2': 'V',
    'voltage_phase_L1N': 'V',
    'voltage_phase_L2L3': 'V',
    'voltage_phase_L2N': 'V',
    'voltage_phase_L3L1': 'V',
    'voltage_phase_L3N': 'V'
}

def create_charger_object(host, port):
    """
    The function `create_charger_object` creates an instance of a car charger object with the specified
    host and port.
    
    :param host: The `host` parameter refers to the IP address or hostname of the device you
    want to connect to. It specifies the location of the device on the network
    :param port: The `port` parameter typically refers to the port number on which the server is
    listening for incoming connections. It is a communication endpoint for network connections. In the
    context of your `create_charger_object` function, the `port` parameter is the port number on
    which the car charger device is accepting information.
    :return: An object of the class `alfen_eve_modbus_tcp.CarCharger` with the specified `host` and
    `port` parameters is being returned.
    """
    car_charger = alfen_eve_modbus_tcp.CarCharger(
        host=host,
        port=port)
    return car_charger


def connect_to_charger(car_charger):
    """
    The function `connect_to_charger` attempts to connect to a car charger and retries if unsuccessful.
    
    :param car_charger: The code is a recursive function
    `connect_to_charger` that attempts to connect to a car charger. If the connection fails, it retries
    after a delay of 3 seconds
    :return: The function `connect_to_charger` is returning the `car_charger` object after attempting to
    connect to the charger. If the connection fails, it retries the connection after a delay.
    """
    try:
        car_charger.connect()
        if not car_charger.connected():
            raise Exception("Could not connect to charger")
    except Exception as e:
        logger.error(f"An error occurred: {e}, retrying in 3 seconds...")
        time.sleep(3)
        connect_to_charger(car_charger)
    return car_charger
        

if __name__ == '__main__':
    
    # initialize tgvfunctions object and producer
    tgv = tgvfunctions.TGVFunctions(projectsecrets.TOPIC)
    producer = tgv.make_producer("alfen_eve_charging_station_producer")
    
    def data_pusher():
        """
        The `data_pusher` function reads data from a car charger, processes it, and sends it to a Kafka
        topic at regular intervals.
        """
        try:
            # Try to connect
            car_charger = create_charger_object(host=projectsecrets.MODBUS_HOST, port=projectsecrets.MODBUS_PORT)
            car_charger = connect_to_charger(car_charger)
            # Check if connected
            if not car_charger.connected():
                raise ConnectionError("Car charger is not connected")

            while True:
                try:
                    result = []
                    start_time = time.time() * 1000 
                    # Read all values
                    modbus_result = car_charger.read_all()
                    # Unpack values for mapping
                    for key, value in modbus_result.items():
                        # Check if key is in mapping
                        # to dissable logging of a key comment the key out of key_mapping at the top of the code
                        if key in key_mapping:
                            # Translate the key snake casing to a more readable format
                            measurement_id = key_mapping[key]
                            # Get extra descritpion for the key if there is any
                            description = description_mapping.get(key, '')
                            measurement_description = key
                            # If there was an extra description, add it to the measurement description
                            if description:
                                measurement_description = f"{key} - {description}"
                            # Add the unit out of the unit mapping if there is any
                            unit = unit_mapping.get(key, '')
                            # Create the payload for the key and add it to the total result
                            result.append({
                                "measurement_id": measurement_id,
                                "measurement_description": measurement_description,
                                "unit": unit,
                                "value": value
                            })
                    # Create the payload for the producer with the measurements results and meta data
                    value = {
                            "project_id": "energy",
                            "application_id": "Power Parking",
                            "device_id": "Charging Station Right - Alfen EVE",
                            "device_manufacturer": "Alfen",
                            "device_type": "EVE Single Pro-Line",
                            "device_serial": "ace0015668",
                            "location_id": "Power Parking Right",
                            "timestamp": int(time.time() * 1000),
                            "measurements": result
                        }
                    
                    tgv.produce(producer, value)
                    
                    # Use execution time compensation for sleep time, to achieve close to 1 second intervals
                    finish_time = time.time() * 1000
                    execution_time = max(0, (finish_time - start_time - 1000)/1000)
                    if execution_time < 1:
                        time.sleep(1 - execution_time)
                
                except KeyboardInterrupt:
                    logger.error("Aborted by user")
                    break

                except Exception as e:
                    logger.error(f"An error occurred: {e}")
                    while not car_charger.connected():
                        logger.error(f"Car charger not connected, retrying...")
                        car_charger = create_charger_object(host=projectsecrets.MODBUS_HOST, port=projectsecrets.MODBUS_PORT)
                        car_charger = connect_to_charger(car_charger)
                        time.sleep(1)
                    continue

        except Exception as e:
            logger.error(f"An error occurred: {e}")
              
        finally:
            producer.flush()
 

    data_pusher()
